package rabbitmq

func SendToRabbitMQDLX(object []byte, queueName string, ttl uint) error {
	return Client.PublishOnDLX(object,
		ConfigInfo.DlxExchange,
		queueName,
		ttl)
}

func SendToRabbitMQTopic(object []byte, routerKey, topicName string) error {
	return Client.PublishOnTopic(topicName, routerKey, object)
}
