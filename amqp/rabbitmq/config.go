package rabbitmq

// rabbitmq Config .
type Config struct {
	TTL         uint
	DlxExchange string
}
