package cache

import (
	"log"
	"time"

	"github.com/gomodule/redigo/redis"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

// RedisConn .
var RedisConn *redis.Pool

// InitCache .
func InitCache() {

	if viper.GetString("cache.type") != "redis" {
		log.Println("The cache type must be redis,now is", viper.GetString("cache.type"))
		logrus.Fatal("Unsupported cache type")
	}

	RedisConn = &redis.Pool{
		MaxIdle:     viper.GetInt("cache.max_idle"),
		MaxActive:   viper.GetInt("cache.max_active"),
		IdleTimeout: viper.GetDuration("cache.idle_timeout"),
		Wait:        true,
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial("tcp",
				viper.GetString("cache.host"),
				redis.DialDatabase(viper.GetInt("cache.db")),
				redis.DialPassword(viper.GetString("cache.password")),
			)

			if err != nil {
				logrus.Errorf("cache connect failed: %v", err)
				return nil, err
			}

			return c, err
		},
		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			_, err := c.Do("PING")
			if err != nil {
				logrus.Errorf("database heartbeat failed: %v", err)
			}
			return err
		},
	}

	conn := RedisConn.Get()
	if conn.Err() != nil {
		logrus.Fatalf("%s", conn.Err())
	}
	err := conn.Close()
	if err != nil {
		logrus.Fatalf("%s", conn.Err())
	}
}

// Close .
func Close() error {
	if RedisConn == nil {
		return nil
	}
	return RedisConn.Close()
}

// Set .
func Set(key string, data interface{}, time uint64) error {
	conn := RedisConn.Get()

	defer func() {
		err := conn.Close()
		if err != nil {
			log.Println("close the redis conn err is:", err.Error())
		}
	}()

	_, err := redis.String(conn.Do("SET", key, data))
	if err != nil {
		return err
	}

	if time > 0 {
		_, err = conn.Do("EXPIRE", key, time)
		if err != nil {
			return err
		}
	}

	return nil
}

// Exists .
func Exists(key string) bool {
	conn := RedisConn.Get()
	defer func() {
		err := conn.Close()
		if err != nil {
			log.Println("close the redis conn err is:", err.Error())
		}
	}()

	exists, err := redis.Bool(conn.Do("EXISTS", key))
	if err != nil {
		return false
	}

	return exists
}

// Get .
func Get(key string) ([]byte, error) {
	conn := RedisConn.Get()
	defer func() {
		err := conn.Close()
		if err != nil {
			log.Println("close the redis conn err is:", err.Error())
		}
	}()

	reply, err := redis.Bytes(conn.Do("GET", key))
	if err != nil {
		return nil, err
	}

	return reply, nil
}

// GetString .
func GetString(key string) (string, error) {
	conn := RedisConn.Get()
	defer func() {
		err := conn.Close()
		if err != nil {
			log.Println("close the redis conn err is:", err.Error())
		}
	}()

	reply, err := redis.String(conn.Do("GET", key))
	if err != nil {
		return "", err
	}

	return reply, nil
}

// GetInt64 .
func GetInt64(key string) (uint64, error) {
	conn := RedisConn.Get()
	defer func() {
		err := conn.Close()
		if err != nil {
			log.Println("close the redis conn err is:", err.Error())
		}
	}()

	reply, err := redis.Int64(conn.Do("GET", key))
	if err != nil {
		return 0, err
	}

	return uint64(reply), nil
}

// Delete .
func Delete(key string) (bool, error) {
	conn := RedisConn.Get()
	defer func() {
		err := conn.Close()
		if err != nil {
			log.Println("close the redis conn err is:", err.Error())
		}
	}()

	return redis.Bool(conn.Do("DEL", key))
}

// LikeDeletes .
func LikeDeletes(key string) error {
	conn := RedisConn.Get()
	defer func() {
		err := conn.Close()
		if err != nil {
			log.Println("close the redis conn err is:", err.Error())
		}
	}()

	keys, err := redis.Strings(conn.Do("KEYS", "*"+key+"*"))
	if err != nil {
		return err
	}

	for _, key := range keys {
		_, err = Delete(key)
		if err != nil {
			return err
		}
	}

	return nil
}

// SetIntList .
func SetIntList(key string, data interface{}, time uint64) error {
	conn := RedisConn.Get()
	defer func() {
		err := conn.Close()
		if err != nil {
			log.Println("close the redis conn err is:", err.Error())
		}
	}()

	_, err := redis.Int64(conn.Do("lpush", key, data))
	if err != nil {
		return err
	}

	_, err = conn.Do("EXPIRE", key, time)
	if err != nil {
		return err
	}

	return nil
}

// GetLen .
func GetLen(key string) (uint64, error) {
	conn := RedisConn.Get()
	defer func() {
		err := conn.Close()
		if err != nil {
			log.Println("close the redis conn err is:", err.Error())
		}
	}()

	length, err := conn.Do("llen", key)
	if err != nil {
		return 0, err
	}

	return length.(uint64), nil
}

// GetListInt64Value .
func GetListInt64Value(key string, index uint64) (uint64, error) {
	conn := RedisConn.Get()
	defer func() {
		err := conn.Close()
		if err != nil {
			log.Println("close the redis conn err is:", err.Error())
		}
	}()

	v, err := redis.Int64(conn.Do("lindex", key, index))
	if err != nil {
		return 0, err
	}

	return uint64(v), nil
}

// RemoveOldestValue .
func RemoveOldestValue(key string) error {
	conn := RedisConn.Get()
	defer func() {
		err := conn.Close()
		if err != nil {
			log.Println("close the redis conn err is:", err.Error())
		}
	}()

	_, err := conn.Do("rpop", key)
	if err != nil {
		return err
	}

	return nil
}
