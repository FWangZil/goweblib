package cache

import (
	"fmt"

	"github.com/pkg/errors"

	"github.com/gomodule/redigo/redis"
)

type Lock struct {
	resource string
	token    string
	conn     redis.Conn
	timeout  int
}

func (lock *Lock) tryLock() (ok bool, err error) {
	_, err = redis.String(lock.conn.Do("SET", lock.key(), lock.token, "EX", lock.timeout, "NX"))
	if err == redis.ErrNil {
		// The lock was not successful, it already exists.
		return false, nil
	}
	if err != nil {
		return false, err
	}
	return true, nil
}

func (lock *Lock) Unlock() (err error) {
	_, err = lock.conn.Do("del", lock.key())
	return
}

func (lock *Lock) key() string {
	return fmt.Sprintf("redislock:%s", lock.resource)
}

func (lock *Lock) AddTimeout(exTime int64) (ok bool, err error) {
	ttlTime, err := redis.Int64(lock.conn.Do("TTL", lock.key()))
	if err != nil {
		return false, err
	}
	if ttlTime > 0 {
		_, err := redis.String(lock.conn.Do("SET", lock.key(), lock.token, "EX", int(ttlTime+exTime)))
		if err == redis.ErrNil {
			return false, nil
		}
		if err != nil {
			return false, err
		}
	}
	return true, nil
}

func TryLock(conn redis.Conn, resource string, token string, DefaultTimeout int) (lock *Lock, ok bool, err error) {
	return TryLockWithTimeout(conn, resource, token, DefaultTimeout)
}

func TryLockWithTimeout(conn redis.Conn, resource string, token string, timeout int) (lock *Lock, ok bool, err error) {
	lock = &Lock{resource, token, conn, timeout}

	ok, err = lock.tryLock()

	if !ok || err != nil {
		lock = nil
	}

	return
}

// GetLock 获得临时锁
func GetLock(resource, lockType string, addTimeOut int64) (*Lock, error) {
	DefaultTimeout := 5
	conn := RedisConn.Get()
	lockKey := fmt.Sprintf("%s/%s", resource, lockType)
	lock, ok, err := TryLock(conn, lockKey, "token", DefaultTimeout)
	if err != nil {
		return nil, errors.Wrap(err, "Redis 锁校验出错")
	}
	if !ok {
		return nil, errors.New("获得锁失败")
	}
	ok, err = lock.AddTimeout(addTimeOut)
	if err != nil {
		return nil, errors.Wrap(err, "Redis 锁延时设置出错")
	}
	if !ok {
		return nil, errors.New("获得延时锁失败")
	}
	return lock, nil
}
