package core

import (
	"net/http"

	"github.com/sirupsen/logrus"

	"github.com/gin-gonic/gin"
)

// Context struct.
type Context struct {
	*gin.Context
}

// Success method.
func (c *Context) Success(code interface{}, msg string, data interface{}) {

	ret := gin.H{
		"code": 200,
		"msg":  "SUCCESS",
		"data": data,
	}

	if code != nil {
		ret["code"] = code
	}

	if msg != "" {
		ret["msg"] = msg
	}

	if data != nil {
		logrus.Info("Pay resp is:", data)
	}

	c.JSON(http.StatusOK, ret)
}

// Fail method.
func (c *Context) Fail(err error, httpStatusCodes ...int) {

	logrus.Errorf("%+v", err)

	code := http.StatusBadRequest
	if len(httpStatusCodes) != 0 {
		code = httpStatusCodes[0]
	}
	ret := gin.H{
		"code": code,
		"msg":  err.Error(),
		"data": nil,
	}

	c.AbortWithStatusJSON(code, ret)
}

// SuccessXML method.
func (c *Context) SuccessXML(code interface{}, message string) {

	ret := gin.H{
		"return_code": "SUCCESS",
		"return_msg":  "",
	}

	if code != nil {
		ret["return_code"] = code
	}

	if message != "" {
		ret["return_msg"] = message
	}

	c.XML(http.StatusOK, ret)
}
