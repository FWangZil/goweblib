package core

import (
	"time"

	"github.com/gin-gonic/gin"
)

// HandlerFunc .
type HandlerFunc func(*Context)

// Handle transform HandlerFuc to gin.HandlerFunc
func Handle(handler HandlerFunc) gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx := &Context{
			Context: c,
		}
		handler(ctx)
	}
}

// Ping method
func Ping(c *Context) {
	c.Success(nil, "", "pong")
}

// Time .
func Time(c *Context) {
	c.Success(
		nil, "",
		gin.H{
			"time": time.Now().Unix(),
		})
}
