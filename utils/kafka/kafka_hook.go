package kafka

import (
	"log"
	"time"

	"github.com/Shopify/sarama"
	"github.com/sirupsen/logrus"
)

// Hook .
type Hook struct {
	// Id of the hook
	id string

	// Log levels allowed
	levels []logrus.Level

	// Log entry formatter
	formatter logrus.Formatter

	// sarama.AsyncProducer
	producer sarama.AsyncProducer
}

// HookParams .
type HookParams struct {
	Id        string
	Levels    []logrus.Level
	Formatter logrus.Formatter
	Brokers   []string
	LogTopic  string
	ClientID  string
}

var allLogTopic string

// Create a new Hook.
func NewKafkaHook(param HookParams) (*Hook, error) {
	kafkaConfig := sarama.NewConfig()
	kafkaConfig.Producer.RequiredAcks = sarama.WaitForLocal       // Only wait for the leader to ack
	kafkaConfig.Producer.Compression = sarama.CompressionSnappy   // Compress messages
	kafkaConfig.Producer.Flush.Frequency = 500 * time.Millisecond // Flush batches every 500ms
	kafkaConfig.ClientID = param.ClientID

	producer, err := sarama.NewAsyncProducer(param.Brokers, kafkaConfig)

	if err != nil {
		return nil, err
	}

	// We will just log to STDOUT if we're not able to produce messages.
	// Note: messages will only be returned here after all retry attempts are exhausted.
	go func() {
		for err := range producer.Errors() {
			log.Printf("Failed to send log entry to kafka: %v\n", err)
		}
	}()

	hook := &Hook{
		id:        param.Id,
		levels:    param.Levels,
		formatter: param.Formatter,
		producer:  producer,
	}
	allLogTopic = param.LogTopic
	return hook, nil
}

// Id .
func (hook *Hook) Id() string {
	return hook.id
}

// Levels .
func (hook *Hook) Levels() []logrus.Level {
	return hook.levels
}

// Fire .
func (hook *Hook) Fire(entry *logrus.Entry) error {
	// Check time for partition key
	var partitionKey sarama.ByteEncoder

	// Get field time
	t, _ := entry.Data["time"].(time.Time)

	// Convert it to bytes
	b, err := t.MarshalBinary()

	if err != nil {
		return err
	}

	partitionKey = b

	// Check topics
	// var topics []string

	// if ts, ok := entry.Data["topics"]; ok {
	// 	if topics, ok = ts.([]string); !ok {
	// 		return errors.New("Field topics must be []string")
	// 	}
	// } else {
	// 	return errors.New("Field topics not found")
	// }

	// Format before writing
	b, err = hook.formatter.Format(entry)

	if err != nil {
		return err
	}

	value := sarama.ByteEncoder(b)

	// for _, topic := range topics {
	hook.producer.Input() <- &sarama.ProducerMessage{
		Key:   partitionKey,
		Topic: allLogTopic,
		Value: value,
	}
	// }

	return nil
}
