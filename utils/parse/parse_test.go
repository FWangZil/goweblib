package parse

import (
	"testing"

	"github.com/shopspring/decimal"
)

func TestDecimalToFeeInt(t *testing.T) {

	testDecimal, err := decimal.NewFromString("4.0.0")
	if err != nil {
		t.Error("价格生成失败")
	}
	t.Log("The testDecimal is: ", testDecimal)

	type args struct {
		d decimal.Decimal
	}
	tests := []struct {
		name    string
		args    args
		want    int
		wantErr bool
	}{
		{
			name: "转换测试一",
			args: args{
				d: testDecimal,
			},
			want:    400,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := DecimalToFeeInt(tt.args.d)
			if (err != nil) != tt.wantErr {
				t.Errorf("DecimalToFeeInt() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("DecimalToFeeInt() got = %v, want %v", got, tt.want)
			}
			t.Log("the format int is:", got)
		})
	}
}
