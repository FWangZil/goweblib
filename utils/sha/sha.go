package sha

import (
	"crypto/hmac"
	"crypto/sha1"
	"crypto/sha256"
	"encoding/hex"
)

func Sha1(data string) string {
	sha1Hash := sha1.New()
	sha1Hash.Write([]byte(data))
	return hex.EncodeToString(sha1Hash.Sum([]byte("")))
}

func HMacSha256(data, secret string) string {
	key := []byte(secret)
	h := hmac.New(sha256.New, key)
	h.Write([]byte(data))
	//	fmt.Println(h.Sum(nil))
	return hex.EncodeToString(h.Sum(nil))

	//	fmt.Println(sha)
	// sha := hex.EncodeToString(h.Sum(nil))
	// return base64.StdEncoding.EncodeToString([]byte(sha))
}
