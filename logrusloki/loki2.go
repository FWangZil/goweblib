package logrusloki

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/sirupsen/logrus"
)

type LokiHook struct {
	// Id of the hook
	id string

	// Log levels allowed
	levels []logrus.Level

	// Log entry formatter
	formatter logrus.Formatter

	loki Client
}

func tryConnect(pushUrl string) {
	const maxRetryCount = 15
	logrus.Debug("Connecting to Loki.")
	for retryCount := 1; retryCount <= maxRetryCount; retryCount++ {
		resp, err := http.Get(pushUrl)
		if err == nil {
			if resp.StatusCode != http.StatusNotFound {
				resp.Body.Close()
				logrus.Debug("Connection to Loki successfully established.")
				return
			}
		}

		logrus.Debugf("Loki connection failed with error (retrying %d of %d): %s", retryCount, maxRetryCount, err)
		time.Sleep(3 * time.Second)
	}
	logrus.Fatal("Could not establish connection to Loki.")
}

func NewLokiHook(id, job string, pushUrl string) (*LokiHook, error) {
	tryConnect(pushUrl)
	labelsStruct := struct {
		source string
		job    string
	}{
		source: id,
		job:    job,
	}
	data, err := json.Marshal(labelsStruct)
	if err != nil {
		logrus.Fatal(err)
	}
	conf := ClientConfig{
		PushURL:            pushUrl,
		Labels:             string(data),
		BatchWait:          5 * time.Second,
		BatchEntriesNumber: 10000,
		SendLevel:          INFO,
		PrintLevel:         INFO,
	}

	loki, err := NewClientJson(conf)
	if err != nil {
		return nil, fmt.Errorf("promtail.NewClient: %s\n", err)
	}

	hook := &LokiHook{
		id:   id,
		loki: loki,
	}

	return hook, nil
}

func (hook *LokiHook) Id() string {
	return hook.id
}

func (hook *LokiHook) Levels() []logrus.Level {
	return hook.levels
}

func (hook *LokiHook) Fire(entry *logrus.Entry) error {
	switch entry.Level {
	case logrus.InfoLevel, logrus.TraceLevel:
		hook.loki.Infof(entry.Message)
	case logrus.DebugLevel:
		hook.loki.Debugf(entry.Message)
	case logrus.ErrorLevel, logrus.FatalLevel, logrus.PanicLevel:
		hook.loki.Errorf(entry.Message)
	default:
		hook.loki.Warnf(entry.Message)
	}

	return nil
}
