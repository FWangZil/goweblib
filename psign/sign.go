package psign

import (
	"crypto/md5"
	"crypto/sha256"
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"math/rand"
	"net/url"
	"reflect"
	"sort"
	"strings"
	"time"
)

/*
RandomStr 随机字符串
size 大小
kind 种类	0：纯数字	1：小写字母	2：大写字母	3：数字、大小写字母
*/
func RandomStr(size int, kind int) string {
	ikind, kinds, result := kind, [][]int{{10, 48}, {26, 97}, {26, 65}}, make([]byte, size)
	isAll := kind > 2 || kind < 0
	rand.Seed(time.Now().UnixNano())
	for i := 0; i < size; i++ {
		if isAll { // random ikind
			ikind = rand.Intn(3)
		}
		scope, base := kinds[ikind][0], kinds[ikind][1]
		result[i] = uint8(base + rand.Intn(scope))
	}
	return string(result)
}

// SignStructSha256 按微信支付签名算法生成md5签名，struct的签名字段在tag中以sign定义，如果没定义则不参与签名，详见
// https://open.swiftpass.cn/openapi/doc?index_1=12&index_2=1&chapter_1=709&chapter_2=716
func SignStructSha256(obj interface{}, key string) (sign string) {
	src := helpSignStruct(obj, key)

	sha256Ctx := sha256.New()
	sha256Ctx.Write([]byte(src))
	cipherStr := sha256Ctx.Sum(nil)

	sign = base64.StdEncoding.EncodeToString(cipherStr)

	// sign = strings.ToUpper(hex.EncodeToString(cipherStr))
	return sign
}

// SignStructMd5 按微信支付签名算法生成md5签名，struct的签名字段在tag中以sign定义，如果没定义则不参与签名，详见
// https://pay.weixin.qq.com/wiki/doc/api/wxa/wxa_api.php?chapter=4_3
func SignStructMd5(obj interface{}, key string) (sign string) {
	src := helpSignStruct(obj, key)

	md5Ctx := md5.New()
	md5Ctx.Write([]byte(src))
	cipherStr := md5Ctx.Sum(nil)

	sign = strings.ToUpper(hex.EncodeToString(cipherStr))
	return sign
}

// SignMapMd5 按微信支付签名算法生成md5签名，详见
// https://pay.weixin.qq.com/wiki/doc/api/wxa/wxa_api.php?chapter=4_3
func SignMapMd5(param url.Values, key string) (sign string) {
	var pList = make([]string, 0, 0)
	for key := range param {
		var value = param.Get(key)
		if len(value) > 0 {
			pList = append(pList, key+"="+value)
		}
	}
	sort.Strings(pList)
	if len(key) > 0 {
		pList = append(pList, fmt.Sprintf("key=%s", key))
	}

	var src = strings.Join(pList, "&")
	md5Ctx := md5.New()
	md5Ctx.Write([]byte(src))
	cipherStr := md5Ctx.Sum(nil)

	sign = strings.ToUpper(hex.EncodeToString(cipherStr))
	return sign
}

func helpSignStruct(obj interface{}, key string) string {
	t := reflect.TypeOf(obj)
	v := reflect.ValueOf(obj)

	var pList = make([]string, 0, 0)

	for i := 0; i < t.NumField(); i++ {
		tagName := t.Field(i).Tag.Get("sign")
		if len(tagName) != 0 {
			tagValue := v.Field(i).Interface()
			pList = append(pList, fmt.Sprintf("%s=%v", tagName, tagValue))
		}
	}
	sort.Strings(pList)

	if len(key) > 0 {
		pList = append(pList, fmt.Sprintf("key=%s", key))
	}

	var src = strings.Join(pList, "&")
	return src
}
