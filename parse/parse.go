package parse

import (
	"strconv"

	"github.com/shopspring/decimal"
)

// ParseUint 转换字符串为uint64
func ParseUint(key string) (uint64, error) {
	if key == "" {
		return 0, nil
	}
	id, err := strconv.ParseUint(key, 10, strconv.IntSize)
	return id, err
}

// IsInArray 查找整型数字是否在数组中存在
func IsInArray(seed uint64, all []uint64) bool {
	if len(all) == 0 {
		return false
	}
	for _, v := range all {
		if seed == v {
			return true
		}
	}
	return false
}

// DecimalScoreToInt 积分decimal 转整形
func DecimalScoreToInt(a decimal.Decimal) int {
	// var f float64
	// f = 100.0
	return SystemDecimalToInt(a)
}

// SystemDecimalToInt deciaml to int
func SystemDecimalToInt(a decimal.Decimal) int {
	return DecimalToInt(a, -1)
}

// DecimalToInt deciaml to int,s:-1 floor;s:0 round;s:1 ceil
func DecimalToInt(a decimal.Decimal, s int) int {
	var temp string
	switch {
	case s == 0:
		// FIXME:
		// temp = fmt.Sprintf("%s", a.Round(0))
	case s >= 1:
		// FIXME:
		// temp = fmt.Sprintf("%s", a.Ceil())
	case s <= -1:
		// FIXME:
		// temp = fmt.Sprintf("%s", a.Floor())
	}
	b, _ := strconv.Atoi(temp)
	return b
}
