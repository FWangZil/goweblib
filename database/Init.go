package database

import (
	"log"
	"sync"

	"github.com/FWangZil/gorm2logrus"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gorm.io/gorm"

	"gitlab.com/FWangZil/goweblib/database/mysql"
	"gitlab.com/FWangZil/goweblib/database/pgsql"
)

// db instance
var (
	// DB .
	DB   *gorm.DB
	once sync.Once
)

// Init 初始化数据库
func Init(gormLogger *gorm2logrus.GormLogrus) {
	once.Do(func() {
		switch viper.GetString("db.type") {
		case "mysql":
			DB = mysql.Init(gormLogger)
		case "pg13":
			DB = pgsql.Init(gormLogger)
		default:
			logrus.Fatal("database init failed")
		}
	})
}

// Close method
func Close() error {
	if DB != nil {
		sqlDB, err := DB.DB()
		if err != nil {
			return err
		}
		if err := sqlDB.Close(); err != nil {
			return err
		}
	}

	log.Println("database connect closed")
	return nil
}
