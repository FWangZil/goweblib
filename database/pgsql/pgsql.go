package pgsql

import (
	"fmt"

	"github.com/FWangZil/gorm2logrus"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

// Init 初始化数据库
func Init(gormLogger *gorm2logrus.GormLogrus) *gorm.DB {
	dsn := fmt.Sprintf(
		"host=%s user=%s password=%s dbname=%s port=%d sslmode=disable TimeZone=Asia/Shanghai",
		viper.GetString("db.pg13.host"),
		viper.GetString("db.pg13.user"),
		viper.GetString("db.pg13.password"),
		viper.GetString("db.pg13.dbname"),
		viper.GetInt("db.pg13.port"),
	)

	// 替换日志
	// logger := zapgorm2.New(zap.L())
	// logger.SetAsDefault()

	gormConfig := &gorm.Config{
		DisableForeignKeyConstraintWhenMigrating: true, // GORM 会为关联创建外键约束,在初始化过程中禁用此功能
	}
	if gormLogger != nil {
		gormConfig.Logger = gormLogger
	}

	// https://github.com/go-gorm/postgres
	db, err := gorm.Open(postgres.New(postgres.Config{
		DSN:                  dsn,
		PreferSimpleProtocol: true, // disables implicit prepared statement usage
	}), gormConfig)
	if err != nil {
		logrus.Fatalf("pgsql connect failed: %v", err)
	}

	sqlDB, err := db.DB()
	if err != nil {
		logrus.Fatalf("pgsql connect pool config failed: %v", err)
	}

	// SetMaxIdleConns 设置空闲连接池中连接的最大数量
	sqlDB.SetMaxIdleConns(viper.GetInt("db.pg13.max_idle_conns"))
	// SetMaxOpenConns 设置打开数据库连接的最大数量。
	sqlDB.SetMaxOpenConns(viper.GetInt("db.pg13.max_open_conns"))

	if err = sqlDB.Ping(); err != nil {
		logrus.Fatalf("database heartbeat failed: %v", err)
	}

	if viper.GetString("mode") == "debug" {
		db = db.Debug()
	}

	logrus.Info("pgsql connect successfully")
	return db
}
