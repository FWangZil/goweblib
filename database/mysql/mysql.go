package mysql

import (
	"fmt"

	"github.com/FWangZil/gorm2logrus"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

// Init 初始化数据库
func Init(gormLogger *gorm2logrus.GormLogrus) *gorm.DB {
	dsn := fmt.Sprintf(
		"%s:%s@tcp(%s:%d)/%s?charset=utf8mb4&parseTime=true&loc=Local",
		viper.GetString("db.mysql.user"),
		viper.GetString("db.mysql.password"),
		viper.GetString("db.mysql.host"),
		viper.GetInt("db.mysql.port"),
		viper.GetString("db.mysql.dbname"),
	)

	gormConfig := &gorm.Config{
		DisableForeignKeyConstraintWhenMigrating: true, // GORM 会为关联创建外键约束,在初始化过程中禁用此功能
	}
	if gormLogger != nil {
		gormConfig.Logger = gormLogger
	}
	db, err := gorm.Open(mysql.New(mysql.Config{
		DSN:                       dsn,
		DefaultStringSize:         256,  // string 类型字段的默认长度
		DisableDatetimePrecision:  true, // 禁用 datetime 精度，MySQL 5.6 之前的数据库不支持
		DontSupportRenameIndex:    true, // 重命名索引时采用删除并新建的方式，MySQL 5.7 之前的数据库和 MariaDB 不支持重命名索引
		DontSupportRenameColumn:   true, // 用 `change` 重命名列，MySQL 8 之前的数据库和 MariaDB 不支持重命名列
		SkipInitializeWithVersion: true, // 根据当前 MySQL 版本自动配置
	}), gormConfig)
	if err != nil {
		logrus.Fatalf("mysql connect failed: %v", err)
	}

	sqlDB, err := db.DB()
	if err != nil {
		logrus.Fatalf("mysql connect pool config failed: %v", err)
	}

	// SetMaxIdleConns 设置空闲连接池中连接的最大数量
	sqlDB.SetMaxIdleConns(viper.GetInt("db.mysql.max_idle_conns"))
	// SetMaxOpenConns 设置打开数据库连接的最大数量。
	sqlDB.SetMaxOpenConns(viper.GetInt("db.mysql.max_open_conns"))

	if err = sqlDB.Ping(); err != nil {
		logrus.Fatalf("database heartbeat failed: %v", err)
	}

	if viper.GetString("mode") == "debug" {
		db = db.Debug()
	}

	logrus.Info("mysql connect successfully")
	return db
}
